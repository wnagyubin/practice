var Tool = (function(){
    class Tool{
        constructor(){
            if(window.getComputedStyle){
                this.onOff = true;
            }else{
                this.onOff = false;
            }
        }
        // 获取随机数的方法
        getRandom(min,max){
            // 20~100
            return parseInt(Math.random()*(max-min+1))+min;
        }
        // 获取随机颜色
        getColor(){
            return `rgb(${this.getRandom(0,255)},${this.getRandom(0,255)},${this.getRandom(0,255)})`;
        }
        // 获取样式
        getStyle(ele,attr){
            if(this.onOff){
                return window.getComputedStyle(ele)[attr];
            }else{
                return ele.currentStyle[attr];
            }
        }
        // 设置样式
        setStyle(ele,obj){
            for(var attr in obj){
                ele.style[attr] = obj[attr];
            }
        }
    }
    var obj;
    return function(){
        if(!obj){
            obj = new Tool;
        }
        return obj;
    }
})();