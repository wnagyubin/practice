function sendAjax(obj) {
    var ajax = new XMLHttpRequest();
    // 打开连接
    ajax.open(obj.method, obj.url, obj.async);
    // 监听ajax状态
    ajax.onreadystatechange = function () {
        if (ajax.status == 200 && ajax.readyState == 4) {
            var data = ajax.responseText;
 
            obj.success(data);

        }
    }
    if (obj.method == "post") {
        ajax.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    }
    var str = '';
    var f = '';
    for (var i in obj.data) { // obj.data
        str += f + i + "=" + obj.data[i];
        f = '&';
    }
    ajax.send(str); // &隔开的键值对   键=值&键=值   name=翠花&age=20
}

/**
 * 请求方式
 * 请求地址
 * 同步异步
 * 请求数据
 * 请求成功以后要执行的动作，函数传进去
 */

// var obj = {
//     method:"get",
//     url:"where.php",
//     async:true,
//     data:{
//         name:"翠花",
//         age:20
//     },
//     success:function(){

//     }
// }
// sendAjax();