// 共同的父类 - 获取大盒子
class GetBox{
    constructor(classname){
        this.box = document.querySelector("."+classname);
    }
}
// 选项卡
class Tab extends GetBox{
    constructor(classname){
        super(classname);
        this.span = this.box.querySelectorAll("span");
        this.p = this.box.querySelectorAll("p");
        var _this = this; 
        for(var i=0;i<this.span.length;i++){
            this.span[i].setAttribute("index",i);
            this.span[i].onclick=function(){
                _this.click(this); 
            }
        }
    }
    click(ele){
        var index = ele.getAttribute("index"); 
        for(var i=0;i<this.span.length;i++){
            this.span[i].className = '';
            this.p[i].className = '';
        }
        ele.className = 'current';
        this.p[index].className = 'current';
    }
}

// 轮播图
class Carousel extends GetBox{
    constructor(classname,speed){
        super(classname);
        this.ulis = this.box.querySelectorAll("ul li");
        this.olis = this.box.querySelectorAll("ol li");
        this.left = this.box.querySelector("a.left");
        this.right = this.box.querySelector("a.right");
        this.index = 0;
        this.timer = 1;
        this.speed = speed;
        this.auto();
        var _this = this;
        this.box.addEventListener("mouseover",function(){
            _this.over();
        });
        this.box.addEventListener("mouseout",function(){
            _this.out();
        });
        this.right.addEventListener("click",function(){
            _this.rightSlide();
        });
        this.left.addEventListener("click",function(){
            _this.leftSlide();
        });
        for(var i=0;i<this.olis.length;i++){
            this.olis[i].setAttribute("index",i);
            this.olis[i].addEventListener("click",function(){
                _this.click(this);
            })
        }
    }
    click(ele){
        this.index = ele.getAttribute("index");
        this.slideshow();
    }
    slideshow(){
        if(this.index<0){
            this.index=this.ulis.length-1;
        }
        if(this.index>this.ulis.length-1){
            this.index=0;
        }
        for(var i=0;i<this.ulis.length;i++){
            this.ulis[i].className = '';
            this.olis[i].className = '';
        }
        this.ulis[this.index].className = 'current';
        this.olis[this.index].className = 'current';
    }
    leftSlide(){
        this.index--;
        this.slideshow();
    }
    rightSlide(){
        this.index++;
        this.slideshow();
    }
    over(){
        clearInterval(this.timer);
    }
    out(){
        var _this = this;
        this.timer = setInterval(function(){
            _this.index++;
            _this.slideshow();
        },this.speed);
    }
    auto(){
        var _this = this;
        this.timer = setInterval(function(){
            _this.index++;
            _this.slideshow();
        },this.speed);
    }
}

// 放大镜
class Enlarge extends GetBox{
    constructor(classname){
        super(classname);
        this.middle = this.box.querySelector(".middle");
        this.middleImg = this.box.querySelector(".middle img");
        this.shadow = this.box.querySelector(".middle .shadow");
        this.smallImgs = this.box.querySelectorAll(".small img");
        this.big = this.box.querySelector(".big");
        this.that = this.smallImgs[0];
        var _this = this;
        for(var i=0;i<this.smallImgs.length;i++){
            this.smallImgs[i].addEventListener("click",function(){
                _this.Tab(this);
            });
        }
        this.middle.addEventListener("mouseover",function(){
            _this.over();
        });
        this.middle.addEventListener("mouseout",function(){
            _this.out();
        });
        this.middle.addEventListener("mousemove",function(){
            _this.move();
        });
    }
    move(e){
        var e = e || window.event;
        var x = e.clientX;
        var y = e.clientY;
        var boxLeft = this.box.offsetLeft;
        var boxTop = this.box.offsetTop;
        var shadowBanX = this.shadow.clientWidth/2;
        var shadowBanY = this.shadow.clientHeight/2; 
        if(x<boxLeft+shadowBanX){
            x = boxLeft+shadowBanX;
        }
        if(y<boxTop+shadowBanY){
            y = boxTop+shadowBanY;
        }
        if(x>boxLeft+this.middle.offsetWidth-shadowBanX){
            x=boxLeft+this.middle.offsetWidth-shadowBanX
        }
        if(y>boxTop+this.middle.offsetHeight-shadowBanY){
            y=boxTop+this.middle.offsetHeight-shadowBanY
        }
        this.shadow.style.left = x-this.box.offsetLeft-shadowBanX + 'px'
        this.shadow.style.top = y-this.box.offsetTop-shadowBanY + 'px'
        var xPercent = this.shadow.offsetLeft/this.middle.clientWidth;
        var yPercent = this.shadow.offsetTop/this.middle.clientHeight;
    
        var bigImgWidth = parseInt(window.getComputedStyle(this.big)["background-size"].split(" ")[0]);
        var bigImgHeight = parseInt(window.getComputedStyle(this.big)["background-size"].split(" ")[1]);
        this.big.style.backgroundPosition = -xPercent*bigImgWidth+'px '+-yPercent*bigImgHeight+'px';
    }
    over(){
        this.shadow.style.display='block';
        this.big.style.display='block';
        this.big.style.backgroundImage = `url(images/big${this.middleImg.src.substr(this.middleImg.src.lastIndexOf(".")-1)})`;
    }
    out(){
        this.shadow.style.display='none';
        this.big.style.display='none';
    }
    Tab(img){
        this.that.className = '';
        img.className = 'current';
        this.that = img;
        var dotIndex = img.src.lastIndexOf(".");
        var suffix = img.src.substr(dotIndex-1);
        this.middleImg.src = 'images/middle'+suffix;
    }
}