// 许许多多插件组成的个人插件库！
// 链接即用！
// 炒鸡方便！

// 获取随机色
function getColor() {
    return `rgb(${getRandom(0,255)},${getRandom(0,255)},${getRandom(0,255)})`;
}

// 获取随机数
function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// 让ele运动
function sport(ele, obj, cb) {
    // ele      运动的元素
    // obj      运动到的位置
    // cb       运动结束时才运行的函数（可选）
    var timerObj = {}
    for (let attr in obj) {
        timerObj[attr] = setInterval(function () {
            let currentStyle = parseInt(window.getComputedStyle(ele)[attr]);
            let percent = (obj[attr] - currentStyle) / 10;
            if (percent > 0) {
                percent = Math.ceil(percent);
            } else {
                percent = Math.floor(percent);
            }
            if (currentStyle == obj[attr]) {
                clearInterval(timerObj[attr]);
                delete timerObj[attr]
                var n = 0;
                for (var i in timerObj) {
                    n++;
                }
                if (n == 0) {
                    cb();
                }
            } else {
                ele.style[attr] = currentStyle + percent + 'px';
            }
        }, 30);
    }
}

/*
 * 设置cookie
 * @param {*} key cookie中的键
 * @param {*} value cookie中的值
 * @param {*} indate cookie存在时间
 */
function setCookie(key, value, indate) {
    var date = new Date();
    date.setTime(date.getTime() - 8 * 60 * 60 * 1000 + indate * 1000);
    document.cookie = key + "=" + value + ";expires=" + date + ';path = /';
}

/*
 * 获取cookie的方法
 * @param {*} key 要获取的键
 */
function getCookie(key) {
    var str = document.cookie; // name=翠花; age=20; sex=男
    var arr = str.split("; ");
    var length = arr.length;
    for (var i = 0; i < length; i++) {
        var arr1 = arr[i].split("=");
        if (arr1[0] == key) {
            return arr1[1];
        }
    }
}

/*
 * 删除cookie的方法
 * @param {*} key 要删除的指定的键
 */
function delCookie(key) {
    setCookie(key, "", -1);
}