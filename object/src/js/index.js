var banner = document.querySelector(".banner");
var ul = this.banner.querySelector("ul");
var firstli = ul.children[0];
var lastli = ul.children[ul.children.length - 1];
var ol = this.banner.querySelector("ol");
var btn = this.banner.querySelector(".btn")
var left = this.btn.querySelector(".left");
var right = this.btn.querySelector(".right");
var onOff = true;
var num = 1;
var timer;

ul.style.width = ul.children.length * firstli.offsetWidth + 'px';
ul.style.left = -firstli.offsetWidth + 'px';

timer = setInterval(function () {
    num++;
    sport(ul, {
        left: -firstli.offsetWidth * num
    }, function () {
        if (num == ul.children.length - 1) {
            num = 1
            ul.style.left = -firstli.offsetWidth * num + 'px';
        }
        for (let i = 0; i < ol.children.length; i++) {
            ol.children[i].style.background = "none";
        }
        ol.children[num - 1].style.background = "#fff";
        onoff = true;
    });
}, 4000);

for (let i = 0; i < ol.children.length; i++) {
    ol.children[i].setAttribute("index", i)
    ol.children[i].onclick = function () {
        if (onOff) {
            var index = this.getAttribute("index")
            num = parseInt(index) + 1;
            lunbo()
        }
        onOff = false;
    }
}

banner.onmouseover = function () {
    clearInterval(timer);
}
banner.onmouseout = function () {
    timer = setInterval(function () {
        num++;
        lunbo()
    }, 4000);
}


right.onclick = function () {
    if (onOff) {
        num++;
        lunbo()
    }
    onOff = false;
}
left.onclick = function () {
    if (onOff) {
        num--;
        lunbo()
    }
    onOff = false;
}

function lunbo() {
    sport(ul, {
        left: -firstli.offsetWidth * num
    }, function () {
        if (num == 4) {
            num = 1
            ul.style.left = -firstli.offsetWidth * num + 'px';
        }
        if (num == 0) {
            num = ul.children.length - 2;
            ul.style.left = -firstli.offsetWidth * num + 'px';
        }
        for (let i = 0; i < ol.children.length; i++) {
            ol.children[i].style.background = "none";
        }
        ol.children[num - 1].style.background = "#fff";
        onoff = true;
    });
}

function sport(ele, obj, cb) {
    let timerObj = {}
    for (let attr in obj) {
        timerObj[attr] = setInterval(function () {
            let dangqian = parseInt(window.getComputedStyle(ele)[attr]);
            let bili = (obj[attr] - dangqian) / 10;
            if (bili > 0) {
                bili = Math.ceil(bili);
            } else {
                bili = Math.floor(bili);
            }
            if (dangqian == obj[attr]) {
                clearInterval(timerObj[attr]);
                delete timerObj[attr];
                var n = 0;
                for (var i in timerObj) {
                    n++;
                }
                if (n == 0) {
                    cb();
                }
            } else {
                ele.style[attr] = dangqian + bili + 'px';
            }
        }, 30);
    }
}



$(".nav li").mouseover(function () {
    $(".biaomian").stop().animate({
        left: $(this).index() * 100
    }, 150)
});


var name = getCookie('name');
if (name == 'undefined') {
    document.querySelector(".vip").style.display = 'none';
    document.querySelector(".vip-login").style.display = 'block';

} else {
    document.querySelector(".vip").style.display = 'block';
    document.querySelector(".vip-name").innerText = name;
    document.querySelector(".vip-login").style.display = 'none';
    document.querySelector(".vip-register").style.display = 'none';
}


$(".car").click(function () {
    var u = getCookie("name");
    if (u == undefined) {
        var con = confirm("您还未登录，部分功能不能使用，现在去登录？");
        if (con) {
            location.href = './views/login.html';
            return false;
        } else {
            return false;
        }
    } else {
        location.href = './views/car.html';
    }
})