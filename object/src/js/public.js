$(".nav li").mouseover(function () {
    $(".biaomian").stop().animate({
        left: $(this).index() * 100
    }, 150)
});


var name = getCookie('name');
if (name == 'undefined') {
    document.querySelector(".vip").style.display = 'none';
    document.querySelector(".vip-login").style.display = 'block';

} else {
    document.querySelector(".vip").style.display = 'block';
    document.querySelector(".vip-name").innerText = name;
    document.querySelector(".vip-login").style.display = 'none';
    document.querySelector(".vip-register").style.display = 'none';
}

var timer;
var onoff;
var liTop;
var dog = document.querySelector("#dog");
window.onscroll = function () {
    liTop = document.documentElement.scrollTop || document.body.scrollTop;
    if (liTop >= 600) {
        this.dog.style.display = 'block';
    } else {
        this.dog.style.display = 'none';
    }
    if (!onoff) {
        this.clearInterval(timer);
    }
    onoff = false;
}

dog.addEventListener('click',function () {
    var speed = 10;
    timer = setInterval(function () {
        liTop = liTop - speed;
        document.documentElement.scrollTop = liTop;
        document.body.scrollTop = liTop;
        onoff = true;
        if (liTop <= 0) {
            clearInterval(timer);
        }
    }, 1);
})



$(".car").click(function () {
    var u = getCookie("name");
    if (u == undefined) {
        var con = confirm("您还未登录，部分功能不能使用，现在去登录？");
        if (con) {
            location.href = './login.html';
            return false;
        } else {
            return false;
        }
    } else {
        location.href = './car.html';
    }
})